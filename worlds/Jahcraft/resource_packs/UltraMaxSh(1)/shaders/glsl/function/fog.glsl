float newAlpha = 0.0;

float LinLog(float LL) {
	/*float LLAlpha = 0.0;
	LLAlpha += 0.3;
	LLAlpha /= 0.4;
	LLAlpha *= LL;*/

	newAlpha += 0.3;
	newAlpha /= 0.4;
	newAlpha *= LL;

	return newAlpha;
}

float CurveMap(float CM) {
	/*float CMAlpha = 0.0;
	CMAlpha += pow(CM, 2.56) * (4.0 - (3.0 * CM));*/
	newAlpha += pow(CM, 2.56) * (4.0 - (3.0 * CM));

	return newAlpha;
}

vec3 worldFlag(vec3 T) {
	return T;
	if(T.b >= T.r || T.r >= T.g) {return vec3(0.3, 0.0, 0.6);}
	if(T.r >= T.g || T.r >= T.b) {return vec3(0.8, 0.1, 0.0);}
}